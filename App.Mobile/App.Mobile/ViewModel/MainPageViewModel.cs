﻿using App.Mobile.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace App.Mobile.ViewModel
{
    public class MainPageViewModel
    {
        private readonly IPlatformCookieStore cookieStore;
        public string DeleteUrl { set; get; }

        public MainPageViewModel()
        {
            cookieStore = DependencyService.Get<IPlatformCookieStore>(DependencyFetchTarget.NewInstance);
            cookieStore.Url = "https://google.com";
        }

        public List<Cookie> CurrentCookies
        {
            get
            {
                try
                {
                    return cookieStore.CurrentCookies.ToList();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    return new List<Cookie>();
                }
            }
        }

        public ICommand DumpAllCommand => new Command(() =>
        {
            try
            {
                cookieStore.DumpAllCookiesToLog();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        });

        public ICommand DeleteAllCommand => new Command(() =>
        {
            try
            {
                if (!string.IsNullOrEmpty(DeleteUrl))
                    cookieStore.DeleteAllCookiesForSite(DeleteUrl);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());

            }
        });
    }
}
