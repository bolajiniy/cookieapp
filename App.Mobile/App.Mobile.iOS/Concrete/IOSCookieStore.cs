﻿ 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using App.Mobile.Contract;
using App.Mobile.iOS.Concrete;
using Foundation;

[assembly: Xamarin.Forms.Dependency(typeof(IOSCookieStore))]
namespace App.Mobile.iOS.Concrete
{
    public class IOSCookieStore : IPlatformCookieStore
    {
        private readonly object _refreshLock = new object();
        public string Url { set; get; }

        public IEnumerable<Cookie> CurrentCookies
        {
            get { return RefreshCookies(); }
        }

        public IOSCookieStore()
        {
        }

        private IEnumerable<Cookie> RefreshCookies()
        {
            lock (_refreshLock)
            {
                foreach (var cookie in NSHttpCookieStorage.SharedStorage.Cookies)
                {
                    yield return new Cookie
                    {
                        Comment = cookie.Comment,
                        Domain = cookie.Domain,
                        HttpOnly = cookie.IsHttpOnly,
                        Name = cookie.Name,
                        Path = cookie.Path,
                        Secure = cookie.IsSecure,
                        Value = cookie.Value,
                        // TODO expires? / expired?
                        Version = Convert.ToInt32(cookie.Version)
                    };
                }
            }
        }

        public void DumpAllCookiesToLog()
        {
            if (!CurrentCookies.Any())
            {
                LogDebug("No cookies in your iOS cookie store. Srsly? No cookies? At all?!?");
            }
            CurrentCookies.ToList().ForEach(cookie => LogDebug(string.Format("Cookie dump: {0} = {1}", cookie.Name, cookie.Value)));
        }

        private void LogDebug(string log)
        {
           Console.WriteLine(log);
        }

        public void DeleteAllCookiesForSite(string url)
        {
            var cookieStorage = NSHttpCookieStorage.SharedStorage;
            foreach (var cookie in cookieStorage.CookiesForUrl(new NSUrl(url)).ToList())
            {
                cookieStorage.DeleteCookie(cookie);
            }
            // you MUST call the .Sync method or those cookies may hang around for a bit
            NSUserDefaults.StandardUserDefaults.Synchronize();
        }

    }
}